create table job_posting
(
    id           bigint auto_increment
        primary key,
    category     varchar(255) not null,
    company_name varchar(255) not null,
    date_expired datetime(6)  not null,
    date_posted  datetime(6)  not null,
    description  varchar(255) not null,
    location     varchar(255) not null,
    title        varchar(255) not null
);

