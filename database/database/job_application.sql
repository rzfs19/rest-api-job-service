create table job_application
(
    id             bigint auto_increment
        primary key,
    cover_letter   varchar(3000) null,
    email          varchar(255)  not null,
    first_name     varchar(255)  not null,
    last_name      varchar(255)  not null,
    resume         varchar(255)  not null,
    job_posting_id bigint        not null,
    constraint FKa9y14gfb3f86qg8ljwkfeeho5
        foreign key (job_posting_id) references job_posting (id)
);

