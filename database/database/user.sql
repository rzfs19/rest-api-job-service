create table user
(
    id         bigint auto_increment
        primary key,
    email      varchar(255) not null,
    first_name varchar(255) not null,
    is_enabled bit          null,
    last_name  varchar(255) not null,
    password   varchar(255) null,
    role       varchar(255) null,
    constraint UK_ob8kqyqqgmefl0aco34akdtpe
        unique (email)
);

