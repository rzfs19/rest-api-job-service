create table if not exists user
(
    id         bigint auto_increment
        primary key,
    email      varchar(255) not null,
    first_name varchar(255) not null,
    is_enabled bit          null,
    last_name  varchar(255) not null,
    password   varchar(255) null,
    role       varchar(255) null,
    constraint UK_ob8kqyqqgmefl0aco34akdtpe
        unique (email)
);

INSERT INTO `database`.user (id, email, first_name, is_enabled, last_name, password, role) VALUES (1, 'candidate@email.com', 'M Lukman', true, 'Nor Khakim', '$2a$12$ZPZSWyYlm5DxKrHFsTY63Odh1RCKBe78aMZbAzzUCn6RIcLrpgv0S', 'Candidate');
INSERT INTO `database`.user (id, email, first_name, is_enabled, last_name, password, role) VALUES (2, 'company@email.com', 'PT Software', true, 'Engineer', '$2a$12$ZPZSWyYlm5DxKrHFsTY63Odh1RCKBe78aMZbAzzUCn6RIcLrpgv0S', 'Company');

create table if not exists job_posting
(
    id           bigint auto_increment
        primary key,
    category     varchar(255) not null,
    company_name varchar(255) not null,
    date_expired datetime(6)  not null,
    date_posted  datetime(6)  not null,
    description  varchar(255) not null,
    location     varchar(255) not null,
    title        varchar(255) not null
);

INSERT INTO `database`.job_posting (id, category, company_name, date_expired, date_posted, description, location, title) VALUES (1, 'IT', 'ABC Company', '2023-03-01 00:00:00', '2023-05-01 09:00:00', 'Backend Developer', 'Jakarta', 'Backend Developer');
INSERT INTO `database`.job_posting (id, category, company_name, date_expired, date_posted, description, location, title) VALUES (2, 'Marketing', 'XYZ Company', '2023-06-30 00:00:00', '2023-05-01 10:00:00', 'Marketing Manager', 'Bandung', 'Marketing Manager');
INSERT INTO `database`.job_posting (id, category, company_name, date_expired, date_posted, description, location, title) VALUES (3, 'Finance', 'DEF Company', '2023-07-30 00:00:00', '2023-05-02 11:00:00', 'Accounting Staff', 'Surabaya', 'Accounting Staff');
INSERT INTO `database`.job_posting (id, category, company_name, date_expired, date_posted, description, location, title) VALUES (4, 'IT', 'XYZ Company', '2023-06-30 00:00:00', '2023-05-02 12:00:00', 'Frontend Developer', 'Jakarta', 'Frontend Developer');
INSERT INTO `database`.job_posting (id, category, company_name, date_expired, date_posted, description, location, title) VALUES (5, 'Engineering', 'ABC Company', '2023-07-30 00:00:00', '2023-05-03 13:00:00', 'Mechanical Engineer', 'Bandung', 'Mechanical Engineer');


create table if not exists job_application
(
    id             bigint auto_increment
        primary key,
    cover_letter   varchar(3000) null,
    email          varchar(255)  not null,
    first_name     varchar(255)  not null,
    last_name      varchar(255)  not null,
    resume         varchar(255)  not null,
    job_posting_id bigint        not null,
    constraint FKa9y14gfb3f86qg8ljwkfeeho5
        foreign key (job_posting_id) references job_posting (id)
);

INSERT INTO `database`.job_application (id, cover_letter, email, first_name, last_name, resume, job_posting_id) VALUES (1, 'Dear Hiring Manager,

I am excited to apply for the Software Engineer position at your esteemed organization. As an experienced software developer with 5 years of experience in the field, I believe I have the skills and expertise necessary to make a significant contribution to your team.

My experience working in software development has given me a strong understanding of programming languages, software architecture, and database management. Throughout my career, I have worked on a variety of software development projects, ranging from small-scale applications to large-scale enterprise solutions.

In addition to my technical skills, I am also a strong communicator and collaborator. I enjoy working with others and believe that effective communication is crucial to the success of any project. I am confident that my ability to work well in a team environment, combined with my technical expertise, makes me an ideal candidate for this position.

Thank you for considering my application. I look forward to the opportunity to discuss my qualifications further.

Sincerely,
M Lukman Nor Khakim', 'candidate@email.com', 'M Lukman', 'Nor Khakim', 'https://spaceresume.sgp1.digitaloceanspaces.com/files/2/RESUME%20-%20M%20Lukman%20Nor%20Khakim.pdf', 2);