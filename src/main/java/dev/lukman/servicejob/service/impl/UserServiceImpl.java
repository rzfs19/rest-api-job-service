package dev.lukman.servicejob.service.impl;

import dev.lukman.servicejob.model.CustomUserDetail;
import dev.lukman.servicejob.model.User;
import dev.lukman.servicejob.repository.UserRepository;
import dev.lukman.servicejob.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public CustomUserDetail loadUserByUsername(String username) throws UsernameNotFoundException {
        Set<GrantedAuthority> authorities = new HashSet<>();
        CustomUserDetail customUserDetail = new CustomUserDetail();
        Optional<User> user = userRepository.findByEmail(username);
        if (user.isEmpty()){
            throw new UsernameNotFoundException("User not found");
        }
        User data = user.get();
        authorities.add(new SimpleGrantedAuthority(data.getRole()));
        customUserDetail.setAuthorities(authorities);
        customUserDetail.setUser(data);
        return customUserDetail;
    }
}
