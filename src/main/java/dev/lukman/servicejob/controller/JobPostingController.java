package dev.lukman.servicejob.controller;

import dev.lukman.servicejob.constant.ApiConstant;
import dev.lukman.servicejob.service.JobPostingServices;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import java.util.Map;

@RestController
@RequestMapping(ApiConstant.API_JOB)
@RequiredArgsConstructor
public class JobPostingController extends ApiConstant {

    private final JobPostingServices jobPostingServices;

    /**
     * @param search
     * @param category
     * @return
     * @throws Exception
     */
    @GetMapping
    public ResponseEntity<Object> getAllJob(@RequestParam(required = false) String search,
                                           @RequestParam(required = false) String category) throws Exception {
        return jobPostingServices.fetchAll(search, category);
    }

    @PostMapping(value = APPLY_JOB, consumes = { "multipart/form-data" })
    public ResponseEntity<Object> applyJob(@RequestParam(required = true, name = "idJob") Long id,
                                           @RequestParam(required = true, name = "resume") MultipartFile resume,
                                           @RequestParam(required = false) Map<String, String> coverLetter,
                                           @RequestHeader("X-USER-EMAIL") String email
                                           ) throws Exception {
        if (resume.isEmpty()) {
            throw new MissingServletRequestPartException("resume");
        }

        return jobPostingServices.applyJob(id, resume, coverLetter.get("cover_letter"), email);
    }

    @GetMapping(GET_APPLICANT)
    public ResponseEntity<Object> getCandidate(@RequestParam(required = true, name = "idJob") Long id) throws Exception {
        return jobPostingServices.getCandidate(id);
    }

}
