package dev.lukman.servicejob.constant;

public class ApiConstant {
    public static final String API_USER = "/user";
    public static final String API_JOB = "/jobPostings";
    public static final String APPLY_JOB = "/apply-job";
    public static final String GET_APPLICANT = "/applicant";
}
