package dev.lukman.servicejob.repository;

import dev.lukman.servicejob.model.JobPosting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobPostingRepository extends JpaRepository<JobPosting, Long> {


    List<JobPosting> findAllByCategory(String category);

    @Query("SELECT j FROM JobPosting j WHERE j.title LIKE %:keyword% OR j.companyName LIKE %:keyword% OR j.location LIKE %:keyword% AND j.category = :category")
    List<JobPosting> findByKeywordAndCategory(@Param("keyword") String keyword, @Param("category") String category);

    @Query("SELECT j FROM JobPosting j WHERE j.title LIKE %:keyword% OR j.companyName LIKE %:keyword% OR j.location LIKE %:keyword%")
    List<JobPosting> findByKeyword(@Param("keyword") String keyword);

}
