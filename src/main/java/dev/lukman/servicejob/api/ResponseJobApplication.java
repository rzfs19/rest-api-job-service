package dev.lukman.servicejob.api;

import dev.lukman.servicejob.dto.JobApplicationDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseJobApplication {
    private String status;

    private List<JobApplicationDTO> data;

    public static ResponseJobApplication success(List<JobApplicationDTO> data){
        return new ResponseJobApplication("success fetch applicant", data);
    }
}
