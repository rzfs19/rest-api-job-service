package dev.lukman.servicejob.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import dev.lukman.servicejob.dto.JobPostingDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SuccessResponse {
    private String status;
    private int code;
    private String message;
    public static SuccessResponse success(String success, int code, String message){
        return new SuccessResponse(success, code, message);
    }
}
