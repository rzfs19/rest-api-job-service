package dev.lukman.servicejob.api;

import dev.lukman.servicejob.dto.JobPostingDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseJobPosting {
    private String status;

    private List<JobPostingDTO> data;

    public static ResponseJobPosting success(List<JobPostingDTO> data){
        return new ResponseJobPosting("success fetch job", data);
    }


}
