package dev.lukman.servicejob.filter;

import org.springframework.beans.factory.annotation.Value;

public class KeyConstant {

    @Value("${jwt.issuer}")
    public static String ISSUER;

    @Value("${jwt.secret}")
    public static String JWT_SECRET;

    @Value("${jwt.expiration}")
    public static String JWT_EXPIRATION_MS;
}
