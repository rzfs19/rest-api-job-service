package dev.lukman.servicejob.dto;

import dev.lukman.servicejob.model.JobApplication;
import lombok.Data;

@Data
public class JobApplicationDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String resume;
    private String coverLetter;

    public JobApplicationDTO() {}
    public JobApplicationDTO(JobApplication jobApplication) {
        this.id = jobApplication.getId();
        this.firstName = jobApplication.getFirstName();
        this.lastName = jobApplication.getLastName();
        this.email = jobApplication.getEmail();
        this.resume = jobApplication.getResume();
        this.coverLetter = jobApplication.getCoverLetter();
    }
}