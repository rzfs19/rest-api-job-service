package dev.lukman.servicejob.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import dev.lukman.servicejob.model.JobApplication;
import dev.lukman.servicejob.model.JobPosting;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CredentialDTO {
    private String email;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;

    public JobApplication toJobApplication(JobPosting jobPosting, String resume, String coverLetter){
        return new JobApplication()
                .setEmail(email)
                .setFirstName(firstName)
                .setLastName(lastName)
                .setJobPosting(jobPosting)
                .setResume(resume)
                .setCoverLetter(coverLetter);
    }
}
