package dev.lukman.servicejob.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class JobPostingDTO {
    private Long id;

    private String title;

    private String description;

    private String category;

    private String location;

    @JsonProperty("company_name")
    private String companyName;

    @JsonProperty("date_posted")
    private Date datePosted;

    @JsonProperty("date_expired")
    private Date dateExpired;
}
